import git
import os
import shutil
from datetime import datetime

def test_task_part1(path_in, log_path, repositories):
    with open(log_path, "a") as log:
        log.write(f"Function started by {datetime.now()}\n")
        for url, branches in repositories:
            repo_name = url.split('/')[-1]

            for branch in branches:
                path = f"{path_in}/{repo_name}_{branch}" 

                if os.path.exists(path):
                    shutil.rmtree(path)

                os.mkdir(path)

                repo = git.Repo.clone_from(url, path)
                repo.git.checkout(branch)
                log.write(f"{path} was cloned\n")
                
                filename = "data.txt"
                file = f"{path}/{filename}"

                with open(file, "w") as f:
                    time = str(datetime.now())
                    f.write(time + "\n")
                    f.write(f"{repo_name}/{branch}")


                repo.index.add([file])
                commit_message = "datetime updated"

                repo.index.commit(commit_message)

                repo.remotes.origin.push()
                log.write(f"{path} was pushed\n")

if __name__ == "__main__":

    repositories =  (
            ("https://gitlab.com/test-task3924843/part1/first", ("main", "second")),
            ("https://gitlab.com/test-task3924843/part1/second", ("main",))
    )
    path = "/home/login/downloads"    
    log_path = path + "/logs.txt" 

    test_task_part1(path, log_path, repositories)
